from telegram import Update
from telegram.ext import CommandHandler, ContextTypes

from telegram.ext import (
    Application,
    ConversationHandler,
    MessageHandler,
    filters,
)

from bills import (
    TYPE,
    AMOUNT,
    DESCRIPTION,
    CATEGORY,
    bill,
    type_income,
    amount_value,
    description,
    skip_description,
    category,
    cancel,
)
from expenses import PERIOD, spent, chose_period

from requests import get

from re import search
from random import randint
from bs4 import BeautifulSoup
from gspread import authorize
from oauth2client.service_account import ServiceAccountCredentials
import pandas as pd
import logging
from exchange import rate
import os
from dotenv import load_dotenv
from utils import create_keys_google, SCOPE, POK_DEX, number, numerorango
from logs import log_save, log

load_dotenv()

SECRET_KEY = os.getenv("SECRET_KEY")
DEVELOP_SECRET_KEY = os.getenv("DEVELOP_SECRET_KEY")


# Enable logging
logging.basicConfig(
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s", level=logging.INFO
)

logger = logging.getLogger(__name__)


def list_text(lista):

    organizada = "\n".join(lista)
    return organizada


def dftotex(datos_poke):
    head = datos_poke.columns.tolist()
    head = [t + ":" for t in head]

    text = "No info for petdex"
    if not datos_poke.empty:
        body = datos_poke.iloc[0].tolist()

        body = ["\t\t\t\t" + s + "." for s in body]
        lista = [val for pair in zip(head, body) for val in pair]

        text = list_text(lista)
    return text


def get_debilidades(soup):
    debilidad_full = ["Debilidad: "]
    atributo_full = soup.find_all("div", {"class": "dtm-weaknesses"})
    atributo_full = atributo_full[0].find_all("a")

    for debilidad in atributo_full:
        if (
            str(debilidad.find_all("i", {"class": "extra-damage"}))
            == '[<i class="extra-damage" rel="tooltip" title="Hace daño x 4"></i>]'
        ):
            debilidad = (
                debilidad.getText().replace("\n", "").replace("\t", "").replace(" ", "")
            )
            debilidad_full.append("\t\t\t\t" + "*" + debilidad + ".")

        else:
            debilidad = (
                debilidad.getText().replace("\n", "").replace("\t", "").replace(" ", "")
            )
            debilidad_full.append("\t\t\t\t" + debilidad + ".")

    debilidad_full = list_text(debilidad_full)

    return debilidad_full


def get_api_weakness(soup):
    datos_poke = []

    full_types = soup.get("types")
    if full_types:
        for type in full_types:
            type_name = type.get("type").get("name")
            response = get_api(
                number_poke=soup.get("id"), url=type.get("type").get("url"), poke=False
            )

    damage_relations = response.get("damage_relations")

    datos_poke.append(f"Tipo:\n\t\t\t\t{type_name}")

    if damage_relations:
        weakness_string = ""
        half_damage_string = ""
        half_damage_bad_string = ""
        damage_for_string = ""
        no_damage_string = ""
        no_do_damage_name_string = ""

        half_damage_from = damage_relations.get("half_damage_from")
        for half_damage in half_damage_from:
            half_damage_name = half_damage.get("name")
            half_damage_string += f"\n\t\t\t\t{half_damage_name}."

        datos_poke.append(f"Resistente 1/2 a: {half_damage_string}")

        half_damage_to = damage_relations.get("half_damage_to")
        for half_damage_bad in half_damage_to:
            half_damage_bad_name = half_damage_bad.get("name")
            half_damage_bad_string += f"\n\t\t\t\t{half_damage_bad_name}."

        datos_poke.append(f"daño 1/2 contra: {half_damage_bad_string}")

        double_damage_to = damage_relations.get("double_damage_to")
        for damage_for in double_damage_to:
            damage_for_name = damage_for.get("name")
            damage_for_string += f"\n\t\t\t\t{damage_for_name}."

        datos_poke.append(f"Daño x2 a: {damage_for_string}")

        double_damage_from = damage_relations.get("double_damage_from")
        for damage in double_damage_from:
            weakness_name = damage.get("name")
            weakness_string += f"\n\t\t\t\t{weakness_name}."

        datos_poke.append(f"Debilidad x2 a: {weakness_string}")

        no_damage_from = damage_relations.get("no_damage_from")
        for no_damage in no_damage_from:
            no_damage_name = no_damage.get("name")
            no_damage_string += f"\n\t\t\t\t{no_damage_name}."

        datos_poke.append(f"Inmune a: {no_damage_string}")

        no_damage_to = damage_relations.get("no_damage_to")
        for no_do_damage in no_damage_to:
            no_do_damage_name = no_do_damage.get("name")
            no_do_damage_name_string += f"\n\t\t\t\t{no_do_damage_name}."

        datos_poke.append(f"Inutil contra: {no_do_damage_name_string}")

    datos_poke_text = list_text(datos_poke[:10])
    return datos_poke_text


def get_numero(soup):
    numero_full = soup.find_all("div", {"class": "pokedex-pokemon-pagination-title"})
    numero_poke = numero_full[0].find("span", {"class": "pokemon-number"}).getText()
    numero_poke = numero_poke[-3:]
    return numero_poke


def get_tipo(soup):
    atributo_full = soup.find_all("div", {"class": "dtm-type"})
    atributo_full = atributo_full[0].find_all("a")
    tipo_full = ["Tipo: "]

    for tipo in atributo_full:
        tipo = tipo.getText()
        tipo_full.append("\t\t\t\t" + tipo + ".")

        logger.debug(f"tipo pok {tipo}")

    tipo_full = list_text(tipo_full)
    return tipo_full


def get_habilidades(soup):
    datos_poke = []
    atributo_full = soup.find_all("div", {"class": "column-7"})
    for i, atributo in enumerate(atributo_full):
        valor_full = atributo.find_all("li")

        for atri in valor_full:
            try:
                atributo_texto = atri.find(
                    "span", {"class": "attribute-title"}
                ).getText()
            except AttributeError:
                True

            if atributo_texto == "Sexo":

                atributo_valor = atri.find_all("i", {"class": "icon"})

                if len(atributo_valor) == 2:
                    atributo_valor = "M/F"
                elif len(atributo_valor) == 1:
                    if str(atributo_valor) == '[<i class="icon icon_male_symbol"></i>]':
                        atributo_valor = "M"
                    else:
                        atributo_valor = "F"
                else:
                    atributo_valor = "Desconocido"
            else:
                try:
                    atributo_valor = atri.find(
                        "span", {"class": "attribute-value"}
                    ).getText()
                except AttributeError:
                    True
            datos_poke.append(atributo_texto + ":")
            datos_poke.append("\t\t\t\t" + atributo_valor + ".")

    datos_poke_text = list_text(datos_poke[:10])
    return datos_poke_text


def get_api_abilities(soup):
    datos_poke = []

    datos_poke.append(f"Altura:\n\t\t\t\t{soup.get('height')} m.")
    datos_poke.append(f"Peso:\n\t\t\t\t{soup.get('weight')} kg.")

    abilities = soup.get("abilities")

    if abilities:
        ability_string = ""
        for ability in abilities:
            ability_name = ability.get("ability").get("name")
            ability_string += f"\n\t\t\t\t{ability_name}."

        datos_poke.append(f"Habilidad: {ability_string}")

    datos_poke_text = list_text(datos_poke[:10])
    return datos_poke_text


def get_html(poke):
    url = "https://www.pokemon.com/es/pokedex/" + poke.upper()

    # Capturamos el hml de la pagina web y creamos un objeto Response
    r = get(url)
    data = r.text
    # si el request tuvo exito
    status_code = r.status_code
    if status_code == 200:
        # Creamos el objeto soup y le pasamos lo capturado con request
        soup = BeautifulSoup(data, "lxml")
        # Capturamos el titulo de la página y luego lo mostramos
        # Lo que hace BeautifulSoup es capturar lo que esta dentro de la etiqueta title de la url

    else:

        logger.debug(f"Status Code  {status_code}")

    return soup


def get_api(
    number_poke: str, url: str = "https://pokeapi.co/api/v2/pokemon/", poke: bool = True
):
    data = None
    if poke:
        url = f"https://pokeapi.co/api/v2/pokemon/{number_poke}/"

    # Capturamos el hml de la pagina web y creamos un objeto Response
    r = get(url)

    # si el request tuvo exito
    status_code = r.status_code

    if status_code == 200:
        # Creamos el objeto soup y le pasamos lo capturado con request
        data = r.json()
        # Capturamos el titulo de la página y luego lo mostramos
        # Lo que hace BeautifulSoup es capturar lo que esta dentro de la etiqueta title de la url

    else:

        logger.debug(f"Status Code  {status_code}")

    return data


def download_sheet():
    # use creds to create a client to interact with the Google Drive API

    creds = ServiceAccountCredentials.from_json_keyfile_dict(
        keyfile_dict=create_keys_google(), scopes=SCOPE
    )
    client = authorize(creds)

    # Find a workbook by name and open the first sheet
    # Make sure you use the right name here.
    sheet = client.open(POK_DEX)

    # Extract and print all of the values
    # list_of_hashes = sheet.get_all_records()
    # new for second page of sheet
    index_of_sheet = 1

    list_of_hashes = sheet.get_worksheet(index_of_sheet).get_values()

    # convertimos a un dataframe
    datos_full = pd.DataFrame(list_of_hashes, columns=list_of_hashes[0])

    return datos_full


def get_data(poke, datos_full):
    datos_poke = datos_full[datos_full.get("Pokemon") == poke.upper()]
    datos_poke = datos_poke[
        ["Pokemon", "ATT. TYPE", "ELEMENTO", "ROLE", "PERSONALIDAD"]
    ]

    if datos_poke.empty:
        datos_poke = datos_poke[
            ["Pokemon", "ATT. TYPE", "ELEMENTO", "ROLE", "PERSONALIDAD"]
        ]

        logger.debug(f"nombre : {poke}, No existe pokemon")

    return datos_poke


async def push_telegram(context, chat_id, nombre_poke, soup, datos_full):
    datos_poke = get_data(nombre_poke, datos_full)
    numero_poke = soup.get("id")  # get_numero(soup)

    await context.bot.sendMessage(
        chat_id=chat_id,
        text=str(numero_poke) + "\n" + dftotex(datos_poke),
        parse_mode="Markdown",
    )
    await context.bot.sendMessage(
        chat_id=chat_id, text=get_api_abilities(soup), parse_mode="Markdown"
    )
    await context.bot.sendMessage(chat_id=chat_id, text=get_api_weakness(soup))

    return True


def alpha(context, chat_id, user_says) -> bool:
    datos_full = download_sheet()
    datos_poke = get_data(user_says, datos_full)

    if datos_poke.empty:
        context.bot.sendMessage(
            chat_id=chat_id, text="No existe este pokemon: " + user_says
        )
    else:
        soup = get_html(user_says)
        nombre_poke = get_nombre(soup)
        numero_poke = get_numero(soup)
        getimage(context.bot, chat_id, get_url(numero_poke))
        push_telegram(context, chat_id, nombre_poke, soup, datos_full)

    return True


def rand() -> tuple:
    """Generate number str and integer for get data from api and pokemon.com

    Returns:
        tuple:
            (str) number_fix: to url photo
            (int) number_generate to api
    """
    number_generate = randint(1, 809)
    number_fix, validacion = numerorango(number_generate)
    return number_fix, number_generate


def getimage(bot, chat_id, url):
    bot.send_photo(chat_id=chat_id, photo=url)


def get_nombre(soup) -> str:
    nombre = []
    atributo_full = soup.find_all("div", {"class": "pokedex-pokemon-pagination-title"})
    atributo_full = atributo_full[0].find("div").get_text()
    nombre = atributo_full.split("\n")[1].replace(" ", "")
    return nombre


async def pok(update: Update, context: ContextTypes.DEFAULT_TYPE):
    chat_id = update.effective_chat.id

    user_says = " ".join(context.args)

    logger.debug(f"user_says : {user_says}")
    mensaje = "/pok " + str(user_says)
    log_save(update, context, mensaje)
    if user_says.isdigit():

        numero, validacion = number(user_says)

        logger.debug(f"validacion pok {numero}, {validacion}")
        if validacion:
            # soup = get_html(user_says)
            soup = get_api(user_says)
            # nombre_poke = get_nombre(soup)
            nombre_poke = soup.get("name")
            await context.bot.send_photo(chat_id=chat_id, photo=get_url(numero))
            # getimage(context.bot, chat_id, get_url(numero))
            datos_full = download_sheet()
            await push_telegram(context, chat_id, nombre_poke, soup, datos_full)
        else:
            context.bot.send_message(
                chat_id=chat_id,
                text="Solo son 809 no mas que ver, maestro pokemon tu destino ser "
                + user_says,
            )
    elif user_says == "":

        number_poke_str, number_pok_int = rand()

        # soup = get_html(numero_poke) #! eliminate or modify logic

        soup = get_api(number_pok_int)

        nombre_poke = soup.get("name")

        # nombre_poke = get_nombre(soup)

        logger.debug(f"pok: {number_poke_str}, nombre_poke: {nombre_poke}")

        await context.bot.send_photo(chat_id=chat_id, photo=get_url(number_poke_str))
        # getimage(context.bot, chat_id, get_url(numero_poke))
        await context.bot.sendMessage(chat_id=chat_id, text=number_poke_str)

        datos_full = download_sheet()
        await push_telegram(context, chat_id, nombre_poke, soup, datos_full)
        logger.debug('salio de la entrada "/"pok')
        user_says = nombre_poke

    else:

        # alpha(context, chat_id, user_says)#aca

        # def alpha(context, chat_id, user_says):
        datos_full = download_sheet()
        datos_poke = get_data(user_says, datos_full)

        if datos_poke.empty:
            context.bot.sendMessage(
                chat_id=chat_id, text="No existe este pokemon: " + user_says
            )
        else:
            soup = get_api(user_says)

            nombre_poke = soup.get("name")

            # soup = get_html(user_says)
            # nombre_poke = get_nombre(soup)
            # numero_poke = get_numero(soup)

            numero_poke, validation = number(str(soup.get("id")))
            await context.bot.send_photo(chat_id=chat_id, photo=get_url(numero_poke))
            # getimage(context.bot, chat_id, get_url(numero_poke))
            await push_telegram(context, chat_id, nombre_poke, soup, datos_full)

    await context.bot.send_message(
        chat_id=chat_id, text=user_says, parse_mode="Markdown"
    )


def convert_uppercase(update, context):
    logger.debug(f"convert_uppercase : {update.message.text}")
    chat_id = update.effective_chat.id
    context.bot.send_message(
        chat_id=chat_id,
        text=update.message.text.upper()
        + "\n Intenta escribir /help para obtener ayuda sobre el bot.",
    )

    log_save(update, context, update.message.text)


def get_url(numero: str | int) -> str:
    numero_poke = numero
    # contents = get("https://assets.pokemon.com/assets/cms2/img/pokedex/full/" + numero_poke + ".png")
    # url = contents['url']
    url = f"https://assets.pokemon.com/assets/cms2/img/pokedex/full/{numero_poke}.png"

    return url


def get_urlperro():
    contents = get("https://random.dog/woof.json").json()
    url = contents["url"]
    return url


def get_image_urlperro() -> str:
    allowed_extension = ["jpg", "jpeg", "png"]
    file_extension = ""
    while file_extension not in allowed_extension:
        url = get_urlperro()
        file_extension = search("([^.]*)$", url).group(1).lower()
    return url


async def bop(update: Update, context: ContextTypes.DEFAULT_TYPE):
    url = get_image_urlperro()
    chat_id = update.effective_chat.id
    await context.bot.send_photo(chat_id=chat_id, photo=url)

    chat_user_client = update.message.from_user
    logger.debug(f"User name client :  {chat_user_client.username}")
    chat_user_bot = context.bot
    logger.debug(f"User name bot :  {chat_user_bot.username}")

    mensaje = "/bop"
    log_save(update, context, mensaje)


async def help(update: Update, context: ContextTypes.DEFAULT_TYPE):
    chat_id = update.effective_chat.id
    await context.bot.send_message(
        chat_id=chat_id,
        text="""
        Bienvenido a @pokeubot,
Puedes registrar todos los gastos e ingresos que tengas, de manera facil y sencilla. Para empezar usa:
        \n /bill - Registrar gasto/ingreso
 /spent - Consultar gastos

Con este bot podras consultar algunas caracteristicas de los pokemones.
Para consultar sobre un pokemon utiliza: \n /pok nombre_pokemon \n /pok numero_pokemon \n /pok
Este ultimo comando retorna las caracteristicas de un pokemon elegido de manera aleatoria.\n /help - Si necesitas ayuda\n Dev: @purohr""",
    )
    mensaje = "/help"

    log_save(context, update, mensaje)


def error(update, context):
    """Log Errors caused by Updates.

    Args:
        update (_type_): _description_
        context (_type_): _description_
    """
    logger.warning('Update "%s" caused error "%s"', update, context.error)


def main():
    application = Application.builder().token(SECRET_KEY).build()

    start_handler = CommandHandler("start", help)
    bop_handler = CommandHandler("bop", bop)
    log_handler = CommandHandler("log", log)
    help_handler = CommandHandler("help", help)
    pok_handler = CommandHandler("pok", pok)
    rate_handler = CommandHandler("rate", rate)
    spent_handler = ConversationHandler(
        entry_points=[CommandHandler("spent", chose_period)],
        states={
            PERIOD: [
                MessageHandler(
                    filters.Regex("^(dia|semana|mes|tres meses|seis meses|año)$"), spent
                )
            ],
        },
        fallbacks=[CommandHandler("cancel", cancel)],
    )

    bill_handler = ConversationHandler(
        entry_points=[CommandHandler("bill", bill)],
        states={
            TYPE: [MessageHandler(filters.Regex("^(Expense|Gain)$"), type_income)],
            AMOUNT: [
                MessageHandler(filters.TEXT & ~filters.COMMAND, amount_value),
                CommandHandler("skip", skip_description),
            ],
            DESCRIPTION: [
                MessageHandler(filters.TEXT, description),
            ],
            CATEGORY: [
                MessageHandler(
                    filters.Regex(
                        "^(comida|regalos|salud|vivienda|transporte|gastos personales|mascotas|servicios|viajes|deuda|Ahorro|Sueldo|Intereses|bonificaciones|Otros)$"
                    ),
                    category,
                )
            ],
        },
        fallbacks=[CommandHandler("cancel", cancel)],
    )

    application.add_handler(start_handler)
    application.add_handler(bop_handler)
    application.add_handler(log_handler)
    application.add_handler(help_handler)
    application.add_handler(pok_handler)
    application.add_handler(rate_handler)
    application.add_handler(bill_handler)
    application.add_handler(spent_handler)
    application.run_polling()


if __name__ == "__main__":
    main()
