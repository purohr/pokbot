from telegram import Update, ReplyKeyboardRemove, ReplyKeyboardMarkup
from telegram.ext import ContextTypes, ConversationHandler
from bills import fire_store
from datetime import datetime, timedelta
import logging

# Enable logging
logging.basicConfig(
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s", level=logging.INFO
)
# set higher logging level for httpx to avoid all GET and POST requests being logged
logging.getLogger("httpx").setLevel(logging.WARNING)

logger = logging.getLogger(__name__)

PERIOD = 0


def time_period(period: str):
    now = datetime.now()
    if period == "dia":
        start_of_day = datetime(now.year, now.month, now.day, 0, 0, 0)
        return start_of_day
    elif period == "semana":
        start_of_week = now - timedelta(days=now.weekday())
        start_of_week = datetime(
            start_of_week.year, start_of_week.month, start_of_week.day, 0, 0, 0
        )
        return start_of_week
    elif period == "mes":
        return datetime(now.year, now.month, 1, 0, 0, 0)
    elif period == "tres meses":
        three_months_ago = now - timedelta(weeks=12)  # Aproximadamente 3 meses
        return datetime(three_months_ago.year, three_months_ago.month, 1, 0, 0, 0)
    elif period == "seis meses":
        six_months_ago = now - timedelta(weeks=24)  # Aproximadamente 6 meses
        return datetime(six_months_ago.year, six_months_ago.month, 1, 0, 0, 0)
    elif period == "año":
        return datetime(now.year, 1, 1, 0, 0, 0)
    else:
        return datetime(now.year, now.month, 1, 0, 0, 0)


async def chose_period(update: Update, context: ContextTypes.DEFAULT_TYPE):

    reply_keyboard = [["dia", "semana"], ["mes", "tres meses"], ["seis meses", "año"]]

    await update.message.reply_text(
        "Hi! Consult all about your money. "
        "Send /cancel to stop talking to me.\n\n"
        "what time period are you consult?",
        reply_markup=ReplyKeyboardMarkup(
            reply_keyboard,
            one_time_keyboard=True,
            input_field_placeholder="Choose a period",
        ),
    )
    text = update.message.text
    user = update.message.from_user

    logger.info("chose period of %s: %s", user.first_name, text)
    return PERIOD


async def spent(update: Update, context: ContextTypes.DEFAULT_TYPE):

    user = update.message.from_user
    user_id = str(user.id)

    text = update.message.text
    start_period = time_period(text)

    data = fire_store.get_registrations(
        user_id=user_id, period=start_period, type="Expense"
    )

    sum = 0
    for doc in data:
        sum += doc.to_dict()["amount"]

    await update.message.reply_text(
        f"Gastos en {start_period.strftime("%d/%m/%Y")} : {sum}",
        parse_mode="Markdown",
        reply_markup=ReplyKeyboardRemove(),
    )
    logger.info("Spent %s: %s", user.first_name, text)
    return ConversationHandler.END
