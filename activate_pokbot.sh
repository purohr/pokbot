#!/bin/bash

# Especifica el directorio del entorno virtual
VENV_PATH="/home/pi/docker/pokbot"

# Añadir un retraso de 10 segundos
sleep 10

git pull origin master

# Activa el entorno virtual
source "$VENV_PATH/.venv/bin/activate"

# Instala dependencias
pip install -r requirements.txt

# Ejecuta el script de Python
python "$VENV_PATH/pokbot.py"