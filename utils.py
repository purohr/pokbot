import os
from dotenv import load_dotenv

load_dotenv()

ROOT_USER = os.getenv("ROOT_USER")
PRIVATE_KEY_ID = os.getenv("PRIVATE_KEY_ID")
PRIVATE_KEY = os.getenv("PRIVATE_KEY")
CLIENT_EMAIL = os.getenv("CLIENT_EMAIL")
CLIENT_ID = os.getenv("CLIENT_ID")
AUTH_URI = os.getenv("AUTH_URI")
TOKEN_URI = os.getenv("TOKEN_URI")
AUTH_PROVIDER_X509_CERT_URL = os.getenv("AUTH_PROVIDER_X509_CERT_URL")
CLIENT_X509_CERT_URL = os.getenv("CLIENT_X509_CERT_URL")


FIRE_PRIVATE_KEY_ID = os.getenv("FIRE_PRIVATE_KEY_ID")
FIRE_PRIVATE_KEY = os.getenv("FIRE_PRIVATE_KEY")
FIRE_CLIENT_EMAIL = os.getenv("FIRE_CLIENT_EMAIL")
FIRE_CLIENT_ID = os.getenv("FIRE_CLIENT_ID")
FIRE_CLIENT_X509_CERT_URL = os.getenv("FIRE_CLIENT_X509_CERT_URL")

SCOPE = [
    "https://spreadsheets.google.com/feeds",
    "https://www.googleapis.com/auth/drive",
]
FILE_BILL = "bills2"  # name sheet file in google drive
POK_DEX = "potdex"  # name sheet file in google drive


def create_keys_google() -> dict:

    my_key = {
        "type": "service_account",
        "project_id": "getdate-json",
        "private_key_id": PRIVATE_KEY_ID,
        "private_key": PRIVATE_KEY,
        "client_email": CLIENT_EMAIL,
        "client_id": CLIENT_ID,
        "auth_uri": AUTH_URI,
        "token_uri": TOKEN_URI,
        "auth_provider_x509_cert_url": AUTH_PROVIDER_X509_CERT_URL,
        "client_x509_cert_url": CLIENT_X509_CERT_URL,
    }

    return my_key


def create_keys_google_firestore() -> dict:
    """Credentials for google firestore

    Returns:
        dict: Credentials for firestore
    """

    my_key = {
        "type": "service_account",
        "project_id": "pokbot-5bfdd",
        "private_key_id": FIRE_PRIVATE_KEY_ID,
        "private_key": FIRE_PRIVATE_KEY,
        "client_email": FIRE_CLIENT_EMAIL,
        "client_id": FIRE_CLIENT_ID,
        "auth_uri": AUTH_URI,
        "token_uri": TOKEN_URI,
        "auth_provider_x509_cert_url": AUTH_PROVIDER_X509_CERT_URL,
        "client_x509_cert_url": FIRE_CLIENT_X509_CERT_URL,
        "universe_domain": "googleapis.com",
    }

    return my_key


def numerorango(numero) -> tuple:
    if (int(numero) >= 1) and (int(numero) < 10):
        numero = str(numero).rjust(3, "0")
        validacion = True
    elif (int(numero) >= 10) and (int(numero) < 100):
        numero = str(numero).rjust(3, "0")
        validacion = True
    elif int(numero) > 809:
        validacion = False
    else:
        numero = str(numero)
        validacion = True
    return numero, validacion


def number(cadena_numero) -> tuple:
    numero = ""
    for p in cadena_numero:
        if p.isdigit():
            numero += p
        else:
            break

    numero, validacion = numerorango(numero)

    return numero, validacion


def positive_number(text):
    """Verifica si un texto representa un número mayor a cero.

    Args:
      texto: El texto a evaluar.

    Returns:
      True si el texto es un número mayor a cero, False en caso contrario.
    """

    # Verificamos si todos los caracteres son dígitos (excepto el punto decimal)
    if text.isdigit() or (text.replace(".", "", 1).isdigit() and "." in text):
        # Si es un número, lo convertimos a float y comparamos con cero
        numero = float(text)
        if numero > 0:

            return numero
        else:
            return False
    else:
        return False
