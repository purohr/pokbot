from telegram import ReplyKeyboardMarkup, ReplyKeyboardRemove, Update
from telegram.ext import ContextTypes, ConversationHandler
from utils import SCOPE, FILE_BILL, create_keys_google, ROOT_USER, positive_number
from firebase import FireStore

from gspread import authorize
from oauth2client.service_account import ServiceAccountCredentials
from datetime import datetime
from logs import log_save
import logging

# Enable logging
logging.basicConfig(
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s", level=logging.INFO
)
# set higher logging level for httpx to avoid all GET and POST requests being logged
logging.getLogger("httpx").setLevel(logging.WARNING)

logger = logging.getLogger(__name__)

TYPE, AMOUNT, DESCRIPTION, CATEGORY = range(4)

fire_store = FireStore()  # init firestore base


def last_filled_row(worksheet, numeric_column: int):
    """find the las row in the column

    Args:
        worksheet (_type_): instance woksheet
        numeric_column (int): seleted column

    Returns:
        _type_: return last register in column seleted
    """
    str_list = worksheet.col_values(numeric_column)
    return len(str_list)


def format_data_to_sheet(
    data: dict, count_expense: int, count_gain: int
) -> tuple[list, str]:
    """ordering the user data and select the correct position in the sheet

    Args:
        data (dict): user data
        count_expense (int): number of row in column expense
        count_gain (int): number of row in column gains

    Returns:
        tuple[list, str]: list ordered and string of position sheet
    """

    row_writte = ""
    time_now = datetime.now().strftime("%d/%m/%Y")
    data_list = [time_now]
    if data.get("income") == "Expense":
        row_writte = f"B{count_expense + 1}"

    elif data.get("income") == "Gain":
        row_writte = f"G{count_gain + 1}"
    else:
        row_writte = f"l{1}"

    del data["income"]

    for key, value in data.items():
        data_list.append(value)

    return data_list, row_writte


def save_in_sheeps(data: dict, keys: dict) -> bool:
    """Save data in a row in google sheeps

    Args:
        data (dict): data to save
        keys (dict): keys for google

    Returns:
        bool: true if saved and false if not saved
    """
    column_expense = 2  # see this field in google sheet
    column_gain = 7  # see this field in google sheet
    page_in_sheet = 0  # page in sheet

    creds = ServiceAccountCredentials.from_json_keyfile_dict(
        keyfile_dict=keys,
        scopes=SCOPE,
    )
    client = authorize(creds)
    sh = client.open(FILE_BILL)
    worksheet = sh.get_worksheet(page_in_sheet)

    count_expenses = last_filled_row(worksheet, column_expense)
    count_gain = last_filled_row(worksheet, column_gain)

    data, row_writte = format_data_to_sheet(
        data=data, count_expense=count_expenses, count_gain=count_gain
    )

    response_sheet = worksheet.update([data], row_writte)

    if response_sheet.get("updatedRange").count(row_writte):

        return True
    else:
        return False


def save_in_fire(data: dict, user_id: str, transaction_type: str) -> bool:

    data["date"] = datetime.now()

    path = fire_store.add_register(
        user_id=user_id, user_data=data, transaction_type=transaction_type
    )
    if path:
        return True
    else:
        return False


async def bill(update: Update, context: ContextTypes.DEFAULT_TYPE):

    reply_keyboard = [
        [
            "Expense",
            "Gain",
        ]
    ]

    await update.message.reply_text(
        "Hi! Register all about your money. "
        "Send /cancel to stop talking to me.\n\n"
        "what type are you record?",
        reply_markup=ReplyKeyboardMarkup(
            reply_keyboard,
            one_time_keyboard=True,
            input_field_placeholder="Expenses or Gains?",
        ),
    )
    mensaje = "/bill"
    log_save(update, context, mensaje)
    return TYPE


async def type_income(update: Update, context: ContextTypes.DEFAULT_TYPE):
    """Stores the selected gender and asks for a photo."""
    user = update.message.from_user

    user_data = context.user_data
    text = update.message.text

    user_data["income"] = text

    logger.info("Type_income of %s: %s", user.first_name, update.message.text)
    await update.message.reply_text(
        "Send me the AMOUNT than you want to register, "
        "this value is numeric, or send /skip if you don't want to.",
        reply_markup=ReplyKeyboardRemove(),
    )

    return AMOUNT


async def amount_value(update: Update, context: ContextTypes.DEFAULT_TYPE):
    """Stores the selected gender and asks for a photo."""
    user = update.message.from_user

    user_data = context.user_data
    text = update.message.text

    number = positive_number(text)
    if number:
        user_data["amount"] = number
    else:
        logger.info("Amount_value of %s: %s", user.first_name, update.message.text)
        await update.message.reply_text(
            f"{text} not is a valid number,\nExample: 3200.34\nBye",
            reply_markup=ReplyKeyboardRemove(),
        )
        return ConversationHandler.END

    logger.info("Amount_value of %s: %s", user.first_name, update.message.text)
    await update.message.reply_text(
        "Perfect! Please send me a description to register, "
        "or send /skip if you don't want to.",
        reply_markup=ReplyKeyboardRemove(),
    )

    return DESCRIPTION


async def description(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    """Stores the info about the user and ends the conversation."""

    user = update.message.from_user

    user_data = context.user_data
    text = update.message.text

    user_data["description"] = text

    if user_data.get("income") == "Expense":
        reply_keyboard = [
            ["comida", "regalos", "salud", "vivienda"],
            ["transporte", "gastos personales", "mascotas"],
            ["servicios", "viajes", "deuda"],
        ]

    elif user_data.get("income") == "Gain":
        reply_keyboard = [
            ["Ahorro", "Sueldo", "Intereses"],
            ["bonificaciones", "Otros"],
        ]

    logger.info("Description of %s: %s", user.first_name, update.message.text)
    await update.message.reply_text(
        "Perfect ! now you can choice a type than describe the value.",
        reply_markup=ReplyKeyboardMarkup(
            reply_keyboard,
            one_time_keyboard=True,
            input_field_placeholder="Describe the register!",
        ),
    )

    return CATEGORY


async def skip_description(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    """Skips the description and go to category."""
    user = update.message.from_user

    user_data = context.user_data
    text = ""

    user_data["description"] = text

    logger.info("User %s did not send a description.", user.first_name)
    await update.message.reply_text(
        "You seem a bit paranoid! At last, tell me something about yourself."
    )

    return CATEGORY


async def category(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    """Choose a category and ends the conversation."""
    user = update.message.from_user

    user_data = context.user_data
    fire_user_data = user_data.copy()
    text = update.message.text

    user_data["category"] = text

    logger.info("Category of %s: %s", user.first_name, update.message.text)

    user_id = str(update.message.from_user.id)

    save_fire = save_in_fire(
        user_id=user_id,
        data=fire_user_data,
        transaction_type=fire_user_data.get("income"),
    )

    if user_id == ROOT_USER:
        saved = save_in_sheeps(data=user_data, keys=create_keys_google())

        if saved:
            await update.message.reply_text("Sheeps created.")
        else:
            await update.message.reply_text("Sheeps Not created.\nSorry! see you soon")

    if save_fire:
        await update.message.reply_text(
            "New register created.\nThank you! see you soon",
            reply_markup=ReplyKeyboardRemove(),
        )
    else:
        await update.message.reply_text(
            "Not created.\nThank you! see you soon",
            reply_markup=ReplyKeyboardRemove(),
        )

    return ConversationHandler.END


async def cancel(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    """Cancels and ends the conversation."""
    user = update.message.from_user
    logger.info("User %s canceled the conversation.", user.first_name)
    await update.message.reply_text(
        "Bye! I hope we can talk again some day.",
        reply_markup=ReplyKeyboardRemove(),
    )

    return ConversationHandler.END
