# Pokbot
Iniciativa de aprendizaje autodidacta, sobre Scraping Web,  APIs de google y bot de telegram usando el framework python-telegram-bot .
Toma valores de pokemons desde la web oficial y desde un sheep en el drive de google y los muestra en telegram.

Pokbot es un bot de telegram escrito en python 3 para extraer informacion de la web oficial de pokemon y del drive de google. Es un bot hecho con fines educativos.

Este bot esta actualmente en desarrollo. no se garantiza el funcionamiento 24/7 y esta disponible para mejoras por parte de terceros.

## Sitio utilizado.
* https://www.pokemon.com/es/

## Licencia.
Pokbot es licenciado bajo una licencia GPLv3

## Muestra.
Este bot actualmente corre en telegram, bajo el nombre [@pokeubot](https://t.me/pokeubot).

## Contacto.
Puede contactarme haciendo comentarios al codigo, o reportando bug.

## Comandos.
* /help (Brinda inforación sobre el bot.)
* /pok nombre-pokemon (Devuelve informacion del pokemon.)
* /pok numero_pokemon (Devuelve inforación del pokemon ingresando el número.)
* /pok (Retorna un pokemon al azar y da su información)

## Uso.
Se requiere instalar las siguientes librerias.
* python-telegram-bot
* Beautiful Soup 4
* Requests
* Pandas
* Ramdom
* gspread
* oauth2client
* lxml

Para instalar las dependencias utiliza en siguiente comando:
```sh
pip install nombre-dependencia
```

Inicia el bot con lo siguiente.
```sh
python3 pokbot.py
```


## creditos 
 * [@pranay414](https://gist.github.com/pranay414)
 * [@dzakyputra](https://github.com/dzakyputra)
 * [@xlanor](https://github.com/xlanor)

