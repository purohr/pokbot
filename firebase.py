import firebase_admin
from firebase_admin import credentials, firestore
from utils import create_keys_google_firestore
import logging
from datetime import timedelta

# Enable logging
logging.basicConfig(
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s", level=logging.INFO
)
# set higher logging level for httpx to avoid all GET and POST requests being logged
logging.getLogger("httpx").setLevel(logging.WARNING)

logger = logging.getLogger(__name__)


class FireStore:

    def __init__(self) -> None:
        self._db = self.__client()

    def __client(self):
        keys = create_keys_google_firestore()
        cred = credentials.Certificate(keys)
        firebase_admin.initialize_app(cred)
        db = firestore.client()

        return db

    def add_register(self, user_id: str, user_data: dict, transaction_type: str):

        response = (
            self._db.collection("users")
            .document(user_id)
            .collection(transaction_type)
            .add(user_data)
        )
        logger.info("Save firestore of %s: %s", user_id, transaction_type)
        return response[1].path

    def get_registrations(self, user_id: str, period: timedelta, type: str = "Expense"):
        """Get data of user.

        Args:
            user_id (str): identefier of user
            period (timedelta): time period
            type (str, optional): Type of get data. Defaults to "Expense".

        Returns:
            _type_: return data of user
        """

        user_ref = self._db.collection("users").document(user_id).collection(type)

        query = user_ref.where("date", ">=", period)

        results = query.stream()

        return results
