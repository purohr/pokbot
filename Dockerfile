FROM python:3.11-alpine3.19

WORKDIR /usr/src/app

RUN apk update && apk add --no-cache --virtual build-dependencies build-base wget g++ gcc libxslt-dev cmake git openssl3 linux-headers openssl-dev


COPY requirements.txt ./
RUN pip3 install --upgrade pip
RUN pip3 install --upgrade setuptools wheel

RUN pip3 install -r requirements.txt


COPY . .

CMD [ "python", "./pokbot.py" ]
