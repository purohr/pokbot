from telegram import Update
from telegram.ext import ContextTypes
from time import strftime
import logging
import os
from dotenv import load_dotenv
from utils import number


load_dotenv()

# Enable logging
logging.basicConfig(
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s", level=logging.INFO
)

logger = logging.getLogger(__name__)

LOG_SECRET = os.getenv("LOG_SECRET")


def leer_log(numero):  # lee las ultimas lineas indicadas en el archivo

    archivo = "./log"
    f = open(archivo, "r")
    lista = []

    for linea in f.readlines():
        lista.append(linea)
    f.close()
    return lista[numero * -1 :]


def agregar_txt(texto):
    texto = str(texto)

    f = open("log", "a")
    f.write(texto + "\n")
    f.close()
    return


def log_save(update, context, mensaje) -> str:
    tiempo = strftime("%c")

    try:
        if getattr(context.effective_chat, "username"):
            chat_user = context.effective_chat.username
        else:
            chat_user = context.effective_chat.id

        text = f"log  {tiempo}, {chat_user},  {mensaje}"

        logger.info(text)
        agregar_txt(f"{tiempo}  '@'{chat_user}  {mensaje}")
    except Exception:
        if getattr(update.effective_chat, "username"):
            chat_id = update.effective_chat.username
        else:
            chat_id = update.effective_chat.id
        text = f"log , {tiempo}, {chat_id}, {mensaje}"
        logger.info(text)
        agregar_txt(f"{tiempo}  {chat_id}  {mensaje}")

    return text


async def log(update: Update, context: ContextTypes.DEFAULT_TYPE):
    chat_id = update.effective_chat.id
    user_says = " ".join(context.args)

    list_user_says = user_says.split(" ")
    message = ""

    if LOG_SECRET in list_user_says:
        if list_user_says[0].isdigit():
            numero, validacion = number(list_user_says[0])
            lista = leer_log(int(numero))
        else:
            numero = 10
            lista = leer_log(numero)
        for elemento in lista:
            await context.bot.send_message(chat_id=chat_id, text=elemento)
        message = "/log" + list_user_says[0]
    log_save(update, context, message)
