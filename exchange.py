from telegram import Update
from telegram.ext import ContextTypes
import requests

import os
from dotenv import load_dotenv

load_dotenv()

OPEN_EXCHANGE_TOKEN = os.getenv("OPEN_EXCHANGE_TOKEN")


def request_url(url: str, token: str = OPEN_EXCHANGE_TOKEN) -> dict:

    headers = {
        "accept": "application/json",
        "Authorization": f"Token {OPEN_EXCHANGE_TOKEN}",
    }

    response = requests.get(url, headers=headers)
    if response.status_code == 200:
        return response.json()
    else:
        return None


async def rate(update: Update, context: ContextTypes.DEFAULT_TYPE):

    BASE_OPEN_EXCHANGE = "https://openexchangerates.org/api/"
    url = BASE_OPEN_EXCHANGE + "latest.json?symbols=GBP,EUR,AED,CAD,COP"

    message = "Error request openexchange."

    response_open_exchange = request_url(url=url)

    base = response_open_exchange.get("base")

    if response_open_exchange:
        message = f"Coin base: {base}\n"

        rates = response_open_exchange.get("rates")

        for key, value in rates.items():
            message += f"{key} : {value}\n"

    chat_id = update.effective_chat.id
    await context.bot.send_message(
        chat_id=chat_id,
        text=message,
    )
    mensaje = "/rate"

    # log_save(context, update, mensaje)
